const botaoEnviar = document.querySelector(".botao-enviar")

botaoEnviar.addEventListener("click", (event) => {
    // Pega o histórico de mensagens
    const msgHist = document.querySelector(".mensagens")

    // Cria um elemento li no documento
    const li = document.createElement("li")

    // Pega o texto da caixa
    const txtArea = document.querySelector(".texto")

    li.className = "mensagem"
    li.innerHTML = `<p class="texto-msg">${txtArea.value}</p>
            <div class="botoes">
                <input type="button" value="Editar" class="Editar botao-msg" onclick="editar(this)")>
                <input type="button" value="Deletar" class="Deletar botao-msg" onclick="deletar(this)")>
            </div>
            <div class="botoes-modo-editar" hidden>
                <input>
                <input type="button" value="Enviar" class="Enviar botao-msg" onclick="editarTexto(this)">
            </div>`
    txtArea.value = ""

    msgHist.appendChild(li)
})

function deletar(elemento) {
    elemento.parentNode.parentNode.remove()
}

function editar(elemento) {
    const parent = elemento.parentNode.parentNode

    // Esconder os botões normais
    parent.querySelector(".botoes").toggleAttribute("hidden")

    // Mostrar os botões de edição
    parent.querySelector(".botoes-modo-editar").toggleAttribute("hidden")
}

function editarTexto(elemento) {
    const parent = elemento.parentNode.parentNode
    const textArea = elemento.parentNode.children[0]

    // Editar o texto da mensagem
    parent.querySelector(".texto-msg").innerText = textArea.value

    // Esconder os elementos de editar novamente
    parent.querySelector(".botoes").toggleAttribute("hidden")

    // Tirar o atributo hidden dos botões Editar e Deletar
    parent.querySelector(".botoes-modo-editar").toggleAttribute("hidden")

    // Reseta o texto
    textArea.value = ""
}